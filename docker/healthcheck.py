import socket
import sys
import yaml
from uuid import UUID

TCP_IP = '127.0.0.1'
TCP_PORT = 8669
BUFFER_SIZE = 1024
MESSAGE = str.encode("xivo/uuid.yml")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE)
data = s.recv(BUFFER_SIZE)
s.close()

response = yaml.safe_load(data.decode().strip().splitlines()[0])
xivo_uuid = UUID(response['uuid'],version=4)
print("received data: ", xivo_uuid)

if isinstance(xivo_uuid, UUID):
    sys.exit(0)
else:
    sys.exit(1)