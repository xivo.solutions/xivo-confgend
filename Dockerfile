## Image to build from sources

FROM python:3.11-slim-bookworm
LABEL com.avencall.authors="dev@avencall.com"

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# Add dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    gettext-base \
    git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install xivo-confgend
WORKDIR /usr/src/confgend

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY setup.py setup.py
COPY xivo_confgen/ xivo_confgen/
COPY bin/ bin/

RUN pip install .

COPY etc/xivo-confgend/ /etc/xivo-confgend/
COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
COPY docker/docker-entrypoint.d/ /docker-entrypoint.d/
COPY docker/healthcheck.py /healthcheck.py

RUN touch /var/log/xivo-confgend.log
RUN mkdir /var/lib/xivo-confgend

WORKDIR /root
# Clean source dir
RUN rm -rf /usr/src/confgend

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

EXPOSE 8669
ENTRYPOINT ["/docker-entrypoint.sh"]

HEALTHCHECK --timeout=5s --start-period=10s --retries=1 CMD python /healthcheck.py