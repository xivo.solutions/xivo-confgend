#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-confgend',
    version='0.1',
    description='XIVO Configurations Generator',
    author='Avencall',
    author_email='dev@avencall.com',
    url='http://www.xivo.solutions/',
    license='GPLv3',
    packages=find_packages(),
    scripts=['bin/xivo-confgend']
)
