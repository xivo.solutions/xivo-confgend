# -*- coding: UTF-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import base64
import hashlib
import os

from xivo_dao.resources.infos import dao as infos_dao


class SipMdsTrunkGenerator(object):
    def __init__(self, asterisk_conf_dao):
        self.asterisk_conf_dao = asterisk_conf_dao
        self.systemname = os.getenv('MDS_NAME', 'default')

    def generate(self):
        mediaservers = self.asterisk_conf_dao.find_all_mediaservers()
        for mds in mediaservers:
            if mds.name != self.systemname:
                for line in self.format_from_mds_user(mds):
                    yield line
                for line in self.format_to_mds_peer(mds):
                    yield line

    def format_from_mds_user(self, mds):
        secret = base64.urlsafe_b64encode(hashlib.sha256(bytearray(infos_dao.get().uuid, 'utf-8')).digest())
        options = {"call-limit": "0",
                   "subscribemwi": "no",
                   "amaflags": "default",
                   "transport": "udp",
                   "regseconds": "0",
                   "secret": secret,
                   "type": "user",
                   "remotesecret": secret,
                   "context": "xds-from-mds"
                   }

        yield '[from-{}]'.format(mds.name)

        for name, value in options.items():
            yield '{} = {}'.format(name, value)

        yield ''

    def format_to_mds_peer(self, mds):
        secret = base64.urlsafe_b64encode(hashlib.sha256(bytearray(infos_dao.get().uuid, 'utf-8')).digest())
        options = {"call-limit": "0",
                   "subscribemwi": "no",
                   "amaflags": "default",
                   "transport": "udp",
                   "regseconds": "0",
                   "secret": secret,
                   "type": "peer",
                   "username": "from-{}".format(self.systemname),
                   "port": "5060",
                   "host": mds.voip_ip,
                   "remotesecret": secret,
                   "fromuser": "from-{}".format(self.systemname),
                   "context": "default",
                   "qualify": "yes"
                   }

        yield '[to-{}]'.format(mds.name)

        for name, value in list(options.items()):
            yield '{} = {}'.format(name, value)

        yield ''
