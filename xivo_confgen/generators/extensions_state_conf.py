# -*- coding: utf-8 -*-

# Copyright (C) 2025 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import os
from xivo_dao import asterisk_conf_dao
from xivo_confgen.generators.util import AsteriskFileWriter


class ExtensionsStateConf:
    def __init__(self, publish_extensions_state=False):
        self._settings = asterisk_conf_dao.find_mds_conf()
        self._my_mds_name = os.getenv('MDS_NAME', 'default')
        self._publish_extensions_state = publish_extensions_state

    def generate(self, output):
        if not self._publish_extensions_state:
            return output

        ast_file = AsteriskFileWriter(output)

        for mds_name, mds_ip in self._settings['mds'].items():
            if mds_name == self._my_mds_name:
                continue

            self._write_mds_config(ast_file, mds_name, mds_ip)

        return output

    def _write_mds_config(self, ast_file, mds_name, mds_ip):
        ast_file.write_section(f'{mds_name}-devicestate')
        ast_file.write_options_no_space([
            ('type', 'outbound-publish'),
            ('server_uri', f'sip:from-{self._my_mds_name}@{mds_ip}'),
            ('outbound_auth', f'to-{mds_name}'),
            ('event', 'asterisk-devicestate')
        ])

        for section_type in ['inbound-publication', 'asterisk-publication']:
            ast_file.write_section(f'from-{mds_name}')
            ast_file.write_options_no_space(self._get_options(section_type, mds_name))

    def _get_options(self, section_type, mds_name):
        if section_type == 'inbound-publication':
            return [
                ('type', section_type),
                (f'event_asterisk-devicestate', f'from-{mds_name}')
            ]

        elif section_type == 'asterisk-publication':
            return [
                ('type', section_type),
                ('devicestate_publish', f'{mds_name}-devicestate'),
                ('device_state', 'yes'),
                ('device_state_filter', '^PJSIP/')
            ]

