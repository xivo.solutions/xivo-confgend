# -*- coding: utf-8 -*-

# Copyright (C) 2011-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import os

from xivo_dao import asterisk_conf_dao
from xivo_dao.helpers.db_utils import session_scope

CC_POLICY_ENABLED = 'generic'
CC_POLICY_DISABLED = 'never'
CC_OFFER_TIMER = 30
CC_RECALL_TIMER = 20
CCBS_AVAILABLE_TIMER = 900
CCNR_AVAILABLE_TIMER = 900
FILE_VERSION = '/usr/share/xivo/XIVO-VERSION'
NONCONFIGURABLE_OPTIONS = 'trust_id_outbound = yes'


class SipConf(object):

    def __init__(self, trunk_generator, user_generator, mds_trunk_generator):
        self.trunk_generator = trunk_generator
        self.user_generator = user_generator
        self.mds_trunk_generator = mds_trunk_generator
        self.systemname = os.getenv('MDS_NAME', 'default')

    def generate(self, output):
        with session_scope():
            self._generate(output)

    def _generate(self, output):
        data_general = asterisk_conf_dao.find_sip_general_settings(self.systemname)
        self._gen_general(data_general, output)
        print(file=output)

        data_auth = asterisk_conf_dao.find_sip_authentication_settings()
        self._gen_authentication(data_auth, output)
        print(file=output)

        self._gen_trunk(output)
        print(file=output)

        self._gen_mds_trunk(output)
        print(file=output)

        data_ccss = asterisk_conf_dao.find_extenfeatures_settings(['cctoggle'])
        ccss_options = self._ccss_options(data_ccss)
        self._gen_user(ccss_options, output)

        print(file=output)

    def _gen_general(self, data_general, output):
        print('[general]', file=output)
        for item in data_general:
            if item['var_val'] is None:
                continue

            if item['var_name'] == "stunaddr":
                continue

            if item['var_name'] in ('register', 'mwi'):
                print(item['var_name'], "=>", item['var_val'], file=output)

            elif item['var_name'] == 'useragent' and item['var_val'] == 'XIVO_VERSION':
                ua_version = self._get_user_agent_version()
                print(item['var_name'], "=", ua_version, file=output)

            elif item['var_name'] == 'prematuremedia':
                var_val = 'yes' if item['var_val'] == 'no' else 'no'
                print(item['var_name'], "=", var_val, file=output)

            elif item['var_name'] not in ['allow', 'disallow']:
                print(item['var_name'], "=", item['var_val'], file=output)

            elif item['var_name'] == 'allow':
                print('disallow = all', file=output)
                print('allow =', item['var_val'], file=output)

        print(NONCONFIGURABLE_OPTIONS, file=output)

    def _gen_authentication(self, data_authentication, output):
        if len(data_authentication) > 0:
            print('\n[authentication]', file=output)
            for auth in data_authentication:
                mode = '#' if auth['secretmode'] == 'md5' else ':'
                print("auth = %s%s%s@%s" % (auth['user'], mode, auth['secret'], auth['realm']), file=output)

    def _gen_trunk(self, output):
        for line in self.trunk_generator.generate():
            print(line, file=output)

    def _gen_mds_trunk(self, output):
        for line in self.mds_trunk_generator.generate():
            print(line, file=output)

    def _gen_user(self, ccss_options, output):
        for line in self.user_generator.generate(ccss_options):
            print(line, file=output)

    def _ccss_options(self, data_ccss):
        if data_ccss:
            ccss_info = data_ccss[0]
            if ccss_info.get('commented') == 0:
                return {
                    'cc_agent_policy': CC_POLICY_ENABLED,
                    'cc_monitor_policy': CC_POLICY_ENABLED,
                    'cc_offer_timer': CC_OFFER_TIMER,
                    'cc_recall_timer': CC_RECALL_TIMER,
                    'ccbs_available_timer': CCBS_AVAILABLE_TIMER,
                    'ccnr_available_timer': CCNR_AVAILABLE_TIMER,
                }

        return {
            'cc_agent_policy': CC_POLICY_DISABLED,
            'cc_monitor_policy': CC_POLICY_DISABLED,
        }

    def _get_user_agent_version(self):
        xivo_version = self._get_xivo_version()
        if xivo_version != '':
            ua_version = 'XiVO PBX %s' % xivo_version
        else:
            ua_version = 'XiVO PBX'

        return ua_version

    def _get_xivo_version(self):
        try:
            with open(FILE_VERSION, 'r') as f:
                xivo_version = f.readline().rstrip()
        except:
            xivo_version = ''

        return xivo_version


def gen_value_line(key, value):
    return u'%s = %s' % (key, unicodify_string(value))


def unicodify_string(to_unicodify):
    if (type(to_unicodify) is bytes):
        result = to_unicodify.decode('utf-8')
    else:
        result = str(to_unicodify)
    return result
