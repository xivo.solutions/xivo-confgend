# -*- coding: utf-8 -*-

# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import unittest
from io import StringIO

from mock import Mock

from xivo_confgen.generators import rtp
from xivo_confgen.generators.tests.util import assert_config_equal


class TestRtpConf(unittest.TestCase):

    def setUp(self):
        self.asterisk_conf_dao = Mock()
        self.rtp_conf = rtp.RtpConf(self.asterisk_conf_dao)
        self.output = StringIO()

    def test_gen_general_default_values(self):
        staticsip = [
            {'filename': 'sip.conf', 'category': 'general', 'var_name': 'stunaddr',
             'var_val': 'stun.l.google.com:19302'}
        ]

        self.rtp_conf._gen_general(staticsip, self.output)

        assert_config_equal(self.output.getvalue(), '''
            rtpstart = 10000
            rtpend = 20000
            icesupport = yes
            turnaddr = stun.l.google.com:19302
        ''')

    def test_gen_general_stun_is_empty(self):
        staticsip = [
            {'filename': 'sip.conf', 'category': 'general', 'var_name': 'stunaddr', 'var_val': None}
        ]

        self.rtp_conf._gen_general(staticsip, self.output)

        assert_config_equal(self.output.getvalue(), '''
            rtpstart = 10000
            rtpend = 20000
            icesupport = yes
        ''')

    def test_turn_disabled(self):
        staticsip = [
            {'filename': 'sip.conf', 'category': 'general', 'var_name': 'stunaddr',
             'var_val': 'stun.l.google.com:19302'}
        ]
        self.rtp_conf = rtp.RtpConf(self.asterisk_conf_dao, turn_enable=False)

        self.rtp_conf._gen_general(staticsip, self.output)

        assert_config_equal(self.output.getvalue(), '''
            rtpstart = 10000
            rtpend = 20000
            icesupport = yes
        ''')

    def test_stun_enabled_and_turn_disabled(self):
        staticsip = [
            {'filename': 'sip.conf', 'category': 'general', 'var_name': 'stunaddr',
             'var_val': 'stun.l.google.com:19302'}
        ]
        self.rtp_conf = rtp.RtpConf(self.asterisk_conf_dao, stun_enable=True, turn_enable=False)

        self.rtp_conf._gen_general(staticsip, self.output)

        assert_config_equal(self.output.getvalue(), '''
            rtpstart = 10000
            rtpend = 20000
            icesupport = yes
            stunaddr = stun.l.google.com:19302
        ''')

    def test_stun_disabled_and_turn_disabled(self):
        staticsip = [
            {'filename': 'sip.conf', 'category': 'general', 'var_name': 'stunaddr',
             'var_val': 'stun.l.google.com:19302'}
        ]
        self.rtp_conf = rtp.RtpConf(self.asterisk_conf_dao, stun_enable=False, turn_enable=False)

        self.rtp_conf._gen_general(staticsip, self.output)

        assert_config_equal(self.output.getvalue(), '''
            rtpstart = 10000
            rtpend = 20000
            icesupport = yes
        ''')
