# -*- coding: utf-8 -*-

# Copyright (C) 2012-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest

from mock import patch, Mock

from xivo_confgen.generators.queues import QueuesConf
from xivo_confgen.generators.tests.util import assert_generates_config


class TestQueuesConf(unittest.TestCase):

    def setUp(self):
        self.queues_conf = QueuesConf()

    @patch('xivo_dao.asterisk_conf_dao.find_queue_penalty_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_queuefeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_groupfeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_general_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_members_settings', Mock(return_value=[]))
    def test_empty_sections(self):
        assert_generates_config(self.queues_conf, '''
            [general]
            allow_wrapup_termination = yes
        ''')

    @patch('xivo_dao.asterisk_conf_dao.find_queue_members_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_penalty_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_queuefeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_groupfeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_general_settings')
    def test_general_section(self, find_queue_general_settings):
        find_queue_general_settings.return_value = [
            {'var_name': 'autofill', 'var_val': 'no'},
        ]

        assert_generates_config(self.queues_conf, '''
            [general]
            allow_wrapup_termination = yes
            autofill = no
        ''')
        find_queue_general_settings.assert_called_once_with()

    @patch('xivo_dao.asterisk_conf_dao.find_queue_penalty_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_general_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_queuefeatures_settings')
    @patch('xivo_dao.asterisk_conf_dao.find_queue_groupfeatures_settings')
    @patch('xivo_dao.asterisk_conf_dao.find_queue_members_settings')
    def test_queues_section(self, find_queue_members_settings, find_queue_groupfeatures_settings,
                            find_queue_queuefeatures_settings):
        find_queue_queuefeatures_settings.return_value = [
            {'name': 'queue1', 'wrapuptime': 0, 'commented': False, 'joinempty': '', 'leaveempty': u''}
        ]
        find_queue_groupfeatures_settings.return_value = [
            {'name': 'group1', 'wrapuptime': 0, 'commented': False, 'joinempty': '', 'leaveempty': u''}
        ]

        find_queue_members_settings.side_effect = \
            lambda x: {
                'queue1': [{'interface': 'Local/id-34@usercallback', 'penalty': 1, 'membername': 'User/4001', 'state_interface': 'hint:4001@default'}],
                'group1': [{'interface': 'Local/id-12@usercallback', 'penalty': 0, 'membername': 'User/1001', 'state_interface': 'hint:1001@default'}]
            }.get(x, '')

        assert_generates_config(self.queues_conf, '''
            [general]
            allow_wrapup_termination = yes

            [queue1]
            wrapuptime = 0
            member => Local/id-34@usercallback,1,User/4001,hint:4001@default

            [group1]
            wrapuptime = 0
            member => Local/id-12@usercallback,0,User/1001,hint:1001@default
        ''')
        find_queue_queuefeatures_settings.assert_called_once_with()
        find_queue_groupfeatures_settings.assert_called_once_with('default')

    @patch('xivo_dao.asterisk_conf_dao.find_queue_penalty_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_general_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_queuefeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_groupfeatures_settings')
    @patch('xivo_dao.asterisk_conf_dao.find_queue_members_settings')
    def test_queuefeatures_section_with_mds(self, find_queue_members_settings, find_queue_groupfeatures_settings):
        self.queues_conf.systemname = "mds1"
        self.queues_conf.mds = {'peername': 'mds1'}

        find_queue_groupfeatures_settings.return_value = []
        find_queue_members_settings.return_value = [
            {'interface': 'SIP/abc', 'penalty': 1, 'state_interface': ''},
        ]

        assert_generates_config(self.queues_conf, '''
            ;Only group configuration for MDS
            [general]
            allow_wrapup_termination = yes

        ''')

    @patch('xivo_dao.asterisk_conf_dao.find_queue_penalty_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_general_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_queuefeatures_settings', Mock(return_value=[]))
    @patch('xivo_dao.asterisk_conf_dao.find_queue_groupfeatures_settings')
    @patch('xivo_dao.asterisk_conf_dao.find_queue_members_settings')
    def test_groupfeatures_section_with_mds(self, find_queue_members_settings, find_queue_groupfeatures_settings):
        self.queues_conf.systemname = "mds1"
        self.queues_conf.mds = {'peername': 'mds1'}

        find_queue_groupfeatures_settings.return_value = [
            {'name': 'group1', 'wrapuptime': 0, 'commented': False, 'joinempty': '', 'leaveempty': u''}
        ]
        find_queue_members_settings.return_value = [
            {'interface': 'Local/id-1@usercallback', 'penalty': 0, 'membername': 'User/1001', 'state_interface': 'hint:1001@default'},
        ]

        assert_generates_config(self.queues_conf, '''
            ;Only group configuration for MDS
            [general]
            allow_wrapup_termination = yes

            [group1]
            wrapuptime = 0
            member => Local/id-1@usercallback,0,User/1001,hint:1001@default
        ''')

        find_queue_groupfeatures_settings.assert_called_once_with('mds1')
        find_queue_members_settings.assert_called_once_with('group1')
