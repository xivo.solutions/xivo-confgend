# -*- coding: utf-8 -*-

# Copyright (C) 2025 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import unittest
from io import StringIO

from mock import MagicMock, Mock, patch

from xivo_confgen.generators.extensions_state_conf import ExtensionsStateConf


class TestExtensionsStateConf(unittest.TestCase):

    def _new_conf(self, publish_extensions_state, mds_conf, my_mds_name):
        with patch('xivo_confgen.generators.extensions_state_conf.asterisk_conf_dao') as mock_dao:
            mock_dao.find_mds_conf = MagicMock(return_value=mds_conf)
            conf = ExtensionsStateConf(publish_extensions_state)
            conf._my_mds_name = my_mds_name

        return conf

    def test_generated_publish_extension_conf(self):
        self.maxDiff = None

        mds_conf = {
            'mds': {
                'default': '192.168.1.1',
                'mds1': '192.168.1.2',
                'mds2': '192.168.1.3'
            }
        }
        extensions_state_conf = self._new_conf(True, mds_conf, 'default')

        output = StringIO()
        extensions_state_conf.generate(output)

        expected_conf = """[mds1-devicestate]
type=outbound-publish
server_uri=sip:from-default@192.168.1.2
outbound_auth=to-mds1
event=asterisk-devicestate

[from-mds1]
type=inbound-publication
event_asterisk-devicestate=from-mds1

[from-mds1]
type=asterisk-publication
devicestate_publish=mds1-devicestate
device_state=yes
device_state_filter=^PJSIP/

[mds2-devicestate]
type=outbound-publish
server_uri=sip:from-default@192.168.1.3
outbound_auth=to-mds2
event=asterisk-devicestate

[from-mds2]
type=inbound-publication
event_asterisk-devicestate=from-mds2

[from-mds2]
type=asterisk-publication
devicestate_publish=mds2-devicestate
device_state=yes
device_state_filter=^PJSIP/
"""

        self.assertEqual(output.getvalue(), expected_conf)

    def test_generated_publish_extension_conf_with_no_mds(self):
        self.maxDiff = None

        mds_conf = {
            'mds': {
            }
        }
        extensions_state_conf = self._new_conf(True, mds_conf, 'default')

        output = StringIO()
        extensions_state_conf.generate(output)

        expected_conf = ""

        self.assertEqual(output.getvalue(), expected_conf)

    def test_generated_publish_extension_conf_is_not_generated(self):
        self.maxDiff = None

        mds_conf = {
            'mds': {
            }
        }
        extensions_state_conf = self._new_conf(False, mds_conf, 'default')

        output = StringIO()
        extensions_state_conf.generate(output)

        expected_conf = ""

        self.assertEqual(output.getvalue(), expected_conf)


