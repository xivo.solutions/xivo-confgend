#!/usr/bin/env bash
set -e

cleanup_build() {
    docker rmi xivoxc/xivo-confgend-test:"$TARGET_VERSION"
}


trap cleanup_build EXIT

if [ -z "$TARGET_VERSION" ]; then
    echo "TARGET_VERSION is not available"
    exit 1
fi

print_important_step() {
    echo "##########"
    echo "## $1"
    echo "#######################"
}

print_important_step "Building Main Image"
docker build --no-cache --build-arg TARGET_VERSION="$TARGET_VERSION" -t xivoxc/xivo-confgend:"$TARGET_VERSION" .

print_important_step "Building Test Image"
docker build --no-cache --build-arg TARGET_VERSION="$TARGET_VERSION" -t xivoxc/xivo-confgend-test:"$TARGET_VERSION" -f tests/Dockerfile.test .
print_important_step "Launching Unit Test"
docker run --rm xivoxc/xivo-confgend-test:"$TARGET_VERSION"